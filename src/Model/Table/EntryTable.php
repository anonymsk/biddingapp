<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class EntryTable extends Table 
{
    // 'sample' finder
    public function findSample(Query $query, array $options) {
        return $query->select(['id', 'user_id', 'prize_id']); 
    }

    public function initialize(array $config)
    {

        $this->belongsTo('User', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Prizes', [
            'foreignKey' => 'prize_id',
            'joinType' => 'INNER',
        ]);
//        $this->hasMany('Prizes', [
//            'foreignKey' => 'prize_id',
//        ]);
//        $this->hasMany('User', [
//            'foreignKey' => 'user_id',
//        ]);
    }
}

