<?php 

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

class UserController extends AppController
{

     public function index()
     {
        $this->set('users', $this->Users->find('all'));
    }

    public function add()
    {
        $user = $this->User->newEntity();
	$session = $this->request->session();
	//var_dump($session->read('Config.user'));
        if ($this->request->is('post')) {
            $user = $this->User->patchEntity($user, $this->request->data);
            if ($this->User->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
		$session->write('Config.user', $user['username']);
                $session->write('Config.user_id', $user['id']);
                return $this->redirect(['controller' => 'Index','action' => 'view']);
                //return $this->redirect(['action' => 'add']);
            }
            $this->Flash->error(__('Unable to add the user.'));
        }
        $this->set('user', $user);
    }

}
