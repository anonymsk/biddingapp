<?php

namespace App\Controller;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class IndexController extends AppController
{
    // prize-lists view & entry page
    public function view()
    {
        // set up username && set view
        $session = $this->request->session();
        $this->set('user',$session->read('Config.user'));
        $this->set('user_id',$session->read('Config.user_id'));

        // set up query && exec query && set array && set view 
        $this->Prizes = TableRegistry::get('Prizes');
        $query = $this->Prizes->find('all', [
            'fields' => ['id','prizename','prizeimageurl']
        ]);
        $res = $query->all();
        $data = $query->toArray();
        //var_dump($data);
        $this->set('data', $data);
    }

}
