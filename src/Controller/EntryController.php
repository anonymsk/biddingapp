<?php

namespace App\Controller;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class EntryController extends AppController
{
    // 投票結果一覧表示
    public function view()
    {
        $this->Entry = TableRegistry::get('Entry');
        $entry = $this->Entry->find()->all();
        var_dump($entry);
        //$this->set('prizes',$prizes['items']);
    }

    // bidding 
    public function bid()
    {
        $entry = $this->Entry->newEntity();
        $session = $this->request->session();
        if ($this->request->is('post')) {
            $entry = $this->Entry->patchEntity($entry, $this->request->data);
            if ($this->Entry->save($entry)) {
                $this->Flash->success(__('The user has been saved.'));
                $session->write('Config.entry_id', $entry['id']);
                $session->write('Config.entry_prize_id', $entry['prize_id']);
                return $this->redirect(['controller' => 'Entry','action' => 'thanks']);
                //return $this->redirect(['action' => 'add']);
            }
            $this->Flash->error(__('Unable to bid.'));
        }
    }

    // thanks
    public function thanks()
    {
        $session = $this->request->session();
        $this->set('user', $session->read('Config.user'));
        $this->set('entry_id',$session->read('Config.entry_id'));
        $this->set('prize_id',$session->read('Config.prize_id'));
    }

}
