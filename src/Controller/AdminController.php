<?php

namespace App\Controller;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

class AdminController extends AppController
{
    // entry view
    public function view()
    {
        // set up username && set view
        $session = $this->request->session();

        // set up query && exec query && set array && set view 
        $this->Entry = TableRegistry::get('Entry');
        $query = $this->Entry->find('all', [
            'fields' => ['id','prize_id','user_id']
        ]);
        $res = $query->all();
        $data = $query->toArray();
        //var_dump($data);
        $this->set('data', $data);
	$this->_main();
    }

    private function _main()
    {
        // table
        //$entry = TableRegistry::get('Entry');
        
        // find
	//$query = $entry->find('all')->contain(['Prizes','User']);
        //$res = $query->all();
        //$data = $query->toArray();

	$connection = ConnectionManager::get('default');
	$results = $connection->execute('select A.id,A.prizename,A.prizeimageurl,COUNT(B.prize_id) AS エントリ数 FROM prizes AS A LEFT JOIN entry AS B ON A.id=B.prize_id GROUP by A.id,A.prizename ORDER by エントリ数 desc')->fetchAll('assoc');
	//var_dump($results);	

	// data set
	$this->set("data",$results);

    }

    public function details()
    {
        //$session = $this->request->session();
        if ($this->request->is('post')) {
	    // get prize_id 
	    $request_id = $this->request->data["prize_id"];

            // get prizedata
            $this->Prizes = TableRegistry::get('Prizes');
            $query = $this->Prizes->find()
		->where(['id' => $request_id])
		->hydrate(false);
            $data = $query->first();
            $this->set('prize', $data);

            // get entry details
	    $sql = "select A.id,B.username,A.created from entry AS A JOIN user AS B ON A.user_id=B.id WHERE prize_id=$request_id ORDER by created;";
            $connection = ConnectionManager::get('default');
            $results = $connection->execute($sql)->fetchAll('assoc');
            $this->set("data",$results);
        }
        
    }
}
